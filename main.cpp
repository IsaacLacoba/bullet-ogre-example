#include <fstream>
#include <iostream>
#include <cmath>
#include <string>
#include <vector>
#include <unistd.h>
#include <random>
#include <chrono>

#include <OgreRoot.h>
#include <OgreLogManager.h>

#include <OgreRenderWindow.h>
#include <OgreWindowEventUtilities.h>
#include <OgreCamera.h>

#include <OgreSceneManager.h>
#include <OgreResourceGroupManager.h>
#include <OgreConfigFile.h>

#include <OgreOverlayContainer.h>
#include <OgreOverlayManager.h>
#include <OgreOverlayElement.h>
#include <OgreTextAreaOverlayElement.h>
#include <OgreFontManager.h>

#include <OgreEntity.h>
#include <OgreVector3.h>
#include <OgreMath.h>
#include <OgreMeshManager.h>

#include <OIS/OISEvents.h>
#include <OIS/OISInputManager.h>
#include <OIS/OISKeyboard.h>
#include <OIS/OISMouse.h>

#include <btBulletDynamicsCommon.h>

using namespace std;

#define PLAYER_CAMERA "PlayerCamera"
#define CAMERA_NODE "cameraNode"
#define WINDOW_NAME "ProyectRenderWindow"
#define TARGET_FPS 60.0

enum class ShapeType{Sphere, Plane};
enum class EventType{game_event, menu_event};

class MotionState : public btMotionState
// From: http://bulletphysics.org/mediawiki-1.5.8/index.php/MotionStates
{
protected:
  Ogre::SceneNode* node;
  btTransform mInitialPosition;

public:
  MotionState(const btTransform &initialPosition, Ogre::SceneNode *node)
  {
    this->node = node;
    mInitialPosition = initialPosition;
  }

  virtual ~MotionState()
  {
    delete node;
  }

  void setNode(Ogre::SceneNode *node)
  {
    node = node;
  }

  virtual void getWorldTransform(btTransform &worldTrans) const
  {
    worldTrans = mInitialPosition;
  }

  virtual void setWorldTransform(const btTransform &worldTrans)
  {
    if(node == nullptr)
      return; // silently return before we set a node

    btQuaternion rot = worldTrans.getRotation();
    node ->setOrientation(rot.w(), rot.x(), rot.y(), rot.z());
    btVector3 pos = worldTrans.getOrigin();
    node ->setPosition(pos.x(), pos.y(), pos.z());
  }
};

class PhysicsManager {
  btBroadphaseInterface* broadphase;
  btSequentialImpulseConstraintSolver* solver;
  btDefaultCollisionConfiguration* collisionConfiguration;
  btCollisionDispatcher* dispatcher;

public:
  btDiscreteDynamicsWorld* dynamicsWorld;

  PhysicsManager(btVector3 gravity=btVector3(0, -3, 0)) {
    broadphase = new btDbvtBroadphase();
    solver = new btSequentialImpulseConstraintSolver();
    collisionConfiguration = new btDefaultCollisionConfiguration();
    dispatcher = new btCollisionDispatcher(collisionConfiguration);

    dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, broadphase,
                                                solver, collisionConfiguration);
    dynamicsWorld->setGravity(gravity);
  }

  btRigidBody* create_rigid_body(ShapeType type,
                                 const btTransform &worldTransform,
                                 Ogre::SceneNode* node, btScalar mass,
                                 btVector3 inertia,
                                 int size=1,
                                 btVector3 coordinates=btVector3(0,1,0)){
    //REFACTOR: makes more generic
    btCollisionShape* shape;
    if(type == ShapeType::Sphere){
      shape = new btSphereShape(size);
      shape->calculateLocalInertia(mass, inertia);
    }
    else {
      shape = new btStaticPlaneShape(coordinates, 1);
    }

    MotionState* motionState = create_motion_state(worldTransform, node);
    btRigidBody::btRigidBodyConstructionInfo
      rigidBodyCI(mass, motionState, shape, inertia);

    btRigidBody* rigidBody = new btRigidBody(rigidBodyCI);
    dynamicsWorld->addRigidBody(rigidBody);

    return rigidBody;
  }


  ~PhysicsManager() {
    delete dynamicsWorld;
    delete solver;
    delete collisionConfiguration;
    delete dispatcher;
    delete broadphase;
  }

private:
  MotionState* create_motion_state(const btTransform &initialPosition,
                                   Ogre::SceneNode *node) {
    return new MotionState(initialPosition, node);
  }

};

class InvalidConfig {};

class GameConfig {
  string basedir;

public:
  Ogre::LogManager* logManager;
  Ogre::Log* systemLog;
  Ogre::Log* gameLog;

  string resource;
  string plugins;
  string ogre;
  string ogre_log;
  string game_log;

  GameConfig(string basedir="config/") : basedir(basedir) {
    plugins = basedir + "plugins.cfg";
    ogre = basedir + "ogre.cfg";
    ogre_log = basedir + "ogre.log";
    game_log = basedir + "game.log";
    resource = basedir + "resources.cfg";

    logManager = new Ogre::LogManager();
    systemLog = logManager->createLog(ogre_log, true, false, false);
    gameLog = logManager->createLog(game_log, false, true, false);

  }
};



class Scene {
  Ogre::SceneManager* manager;

public:
  PhysicsManager& physicsManager_;

  Scene(Ogre::Root* root, PhysicsManager& physicsManager):
    physicsManager_(physicsManager) {

    manager = root->createSceneManager(Ogre::ST_GENERIC);
    manager->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);
  }

  Ogre::SceneNode* create_node(Ogre::String name, Ogre::String parent="") {
    Ogre::SceneNode* parent_node = get_node_by_name(parent);

    return parent_node->createChildSceneNode(name);
  }

  Ogre::Entity* create_entity_and_attach(Ogre::String name,
                                         Ogre::String mesh,
                                         Ogre::String target_node="",
                                         bool cast_shadows=true){
    Ogre::SceneNode* node = get_node_by_name(target_node);
    Ogre::Entity* entity = manager->createEntity(name, mesh);
    entity->setCastShadows(cast_shadows);

    node->attachObject(entity);
    return entity;
  }

  btRigidBody* create_node_and_entity(Ogre::String name,
                                      Ogre::String mesh,
                                      const btTransform &worldTransform,
                                      Ogre::String parent="",
                                      btScalar mass=1,
                                      btScalar size=0,
                                      btVector3 coordinates=btVector3(0,1,0),
                                      btVector3 inertia=btVector3(0,0,0)) {

    Ogre::SceneNode* node = create_node(name, parent);
    Ogre::Entity* entity = create_entity_and_attach(name, mesh, name);


    return physicsManager_.create_rigid_body(ShapeType::Sphere,
                                             worldTransform,node,
                                             mass, inertia,size,
                                             coordinates);
  }


  void create_ground(Ogre::String name="ground",
                     Ogre::String material="Ground",
                     Ogre::String mesh="ground") {

    Ogre::Plane plane(Ogre::Vector3::UNIT_Y, 0);

    Ogre::MeshManager::getSingleton().createPlane(name,
                   Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
                   plane,200,200,1,1,true,1,20,20,Ogre::Vector3::UNIT_Z);

    physicsManager_.create_rigid_body(ShapeType::Plane,
                      btTransform(btQuaternion(0,0,0,1),btVector3(0,-1,0)),
                                  create_node(name), 0, btVector3(0,0,0),
                                  1, btVector3(0,1,0))
      ->setRestitution(1.0);

    Ogre::Entity* groundEntity = create_entity_and_attach(name, mesh, name);
    groundEntity->setMaterialName(material);
  }

  Ogre::Camera* create_camera(Ogre::RenderWindow* window) {
    Ogre::Camera* camera = manager->createCamera(PLAYER_CAMERA);

    camera->setPosition(Ogre::Vector3(0, 80, 80));
    camera->lookAt(Ogre::Vector3(0,0,0));
    camera->setNearClipDistance(5);

    Ogre::Viewport* viewport = window->addViewport(camera);
    viewport->setBackgroundColour(Ogre::ColourValue(0, 0, 0));

    camera->setAspectRatio(Ogre::Real(viewport->getActualWidth()) / Ogre::Real(viewport->getActualHeight()));

    Ogre::SceneNode* cameraNode = create_node(CAMERA_NODE);
    cameraNode->attachObject(camera);

    return camera;
  }

  Ogre::Light* create_light(string name="light.main"){
    manager->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);

    manager->setAmbientLight(Ogre::ColourValue(0.25, 0.25, 0.25));

    Ogre::Light* light = manager->createLight(name);
    light->setPosition(20, 80, 50);
    light->setCastShadows(true);


    return light;
  }

  Ogre::SceneNode* get_node_by_name(Ogre::String node_name="") {
    Ogre::SceneNode* node;
    if (node_name.empty())
      node = manager->getRootSceneNode();
    else
      node = manager->getSceneNode(node_name);

    return node;
  }

  void stepSimulation(Ogre::Real deltaT, int maxSubSteps ){
    physicsManager_.dynamicsWorld->stepSimulation(deltaT, maxSubSteps);

  }
};

class WindowEventListener: public Ogre::WindowEventListener,
                           public OIS::KeyListener,
                           public OIS::MouseListener {
  OIS::InputManager* inputManager;
  OIS::Keyboard* keyboard;
  OIS::Mouse* mouse;

  typedef vector<OIS::KeyCode> KeyCodes;
  KeyCodes keys_pressed_;
  map<KeyCodes, function<void()>> game_triggers_, menu_triggers_;
public:
  bool exit_;

  WindowEventListener(Ogre::RenderWindow* window) {
    exit_ = false;

    create_input_manager(window);

    keyboard = static_cast<OIS::Keyboard*>(inputManager->createInputObject(OIS::OISKeyboard, true));
    mouse = static_cast<OIS::Mouse*>(inputManager->createInputObject(OIS::OISMouse, true));

    keyboard->setEventCallback(this);
    mouse->setEventCallback(this);
    Ogre::WindowEventUtilities::addWindowEventListener(window, this);
  }

  void capture(void) {
    keyboard->capture();
    mouse->capture();
    Ogre::WindowEventUtilities::messagePump();
  }

  void check_events(void){
    if(menu_triggers_[keys_pressed_]){
      menu_triggers_[keys_pressed_]();
      keys_pressed_.clear();
    }
    else if(game_triggers_[keys_pressed_])
      game_triggers_[keys_pressed_]();
  }

  void shutdown() {
    exit_ = true;
    cout << ":bye" << endl;
  }

  void add_hook(WindowEventListener::KeyCodes keystroke, EventType type, function<void()> callback) {
    if(type == EventType::game_event)
      game_triggers_[keystroke] = callback;
    else{
      menu_triggers_[keystroke] = callback;
    }
  }

  void remove_key_from_buffer(OIS::KeyCode keycode) {
    auto key = find (keys_pressed_.begin(), keys_pressed_.end(), keycode);
    if(key != keys_pressed_.end())
      keys_pressed_.erase(key);
  }

  bool keyPressed(const OIS::KeyEvent &arg) {
    keys_pressed_.push_back(arg.key);
    return true;
  }

  bool keyReleased(const OIS::KeyEvent& arg) {
    remove_key_from_buffer(arg.key);
    return true;
  }

  bool mouseMoved(const OIS::MouseEvent&  evt) {
    return true;
  }
  bool mousePressed(const OIS::MouseEvent& evt, OIS::MouseButtonID id) {
    return true;
  }
  bool mouseReleased(const OIS::MouseEvent& evt, OIS::MouseButtonID id) {
    return true;
  }

private:
  void create_input_manager(Ogre::RenderWindow* window) {
    typedef std::pair<string, string> parameter;
    OIS::ParamList parameters;
    size_t xid = 0;

    window->getCustomAttribute("WINDOW", &xid);
    parameters.insert(parameter("WINDOW", to_string(xid)));
    parameters.insert(parameter("x11_mouse_grab", "false"));
    parameters.insert(parameter("x11_mouse_hide", "false"));
    parameters.insert(parameter("x11_keyboard_grab", "false"));
    parameters.insert(parameter("XAutoRepeatOn", "false"));

    inputManager = OIS::InputManager::createInputSystem(parameters);
  }

};

class Game {
  Ogre::Root* root;

  Ogre::Camera* camera;
  Ogre::Light* light;

  Ogre::OverlayManager* overlayManager;
  Ogre::OverlayElement* overlayElement;

  Ogre::Real deltaT;
  Ogre::Real last;
  Ogre::Real time_interval;

  PhysicsManager physicsManager;

  vector<btRigidBody*> balls;

public:
  const GameConfig config;
  Ogre::RenderWindow* window;

  Ogre::Timer* timer;

  Scene* scene;

  Game()  {
    root = new Ogre::Root(config.plugins, config.ogre, config.ogre_log);

    check_config();

    //WARNING!: If load_resources if called before initialize
    //render system, program will segfault while parsing files.overlay
    window = root->initialise(true, WINDOW_NAME);
    load_resources(config.resource);

    scene = new Scene(root, physicsManager);

    scene->create_ground();
    camera = scene->create_camera(window);
    light = scene->create_light();

    create_timer();
  }

  ~Game() {
    delete scene;
    for(btRigidBody* ball: balls)
      delete ball;
  }

  void render() {
    time_interval = time_interval + deltaT;

    Ogre::Real actual_fps =  window->getAverageFPS();

    if(time_interval >= 1000/TARGET_FPS){
      // gui->overlayElement->setCaption(Ogre::StringConverter::toString(actual_fps));
      // config.gameLog->logMessage("FPS: " + to_string(actual_fps));

      root->renderOneFrame();
      time_interval = 0.0;
    }
  }

  void set_camera_mode(Ogre::PolygonMode mode){
    camera->setPolygonMode(mode);
  }

  void start(WindowEventListener& listener) {
    register_keybindings(listener);
    game_loop(listener);
  }

  void create_ball() {
    auto seed = chrono::high_resolution_clock::now().time_since_epoch().count();
    auto random = bind(uniform_real_distribution<>{0,1}, default_random_engine(seed));
    string name = "ball" + to_string(balls.size());
    vector<double> rotation = {random(), random(), random()};

    btRigidBody* ball = scene->create_node_and_entity(name, "ball.mesh",
                             btTransform(btQuaternion(rotation[0],
                                                      rotation[1],
                                                      rotation[2],0.6),
                                         btVector3(0,100,0)),
                             "ground", 80, 5);

    scene->get_node_by_name(name)->setScale(5.0, 5.0, 5.0);

    ball->setRestitution(0.8);
    ball->setFriction(1.5f);
    balls.push_back(ball);
  }

private:
  void check_config(void) {
    if (not (root->restoreConfig() || root->showConfigDialog())) {
      config.systemLog->logMessage("Initialize::configure_ogre => " +
                                   string("ERROR: unable to configure Ogre"));
      throw InvalidConfig();
    }
  }

  void create_timer() {
    timer = root->getTimer();
    timer->reset();
    time_interval = 0.0;
  }

  void load_resources(std::string resources_file) {
    Ogre::ConfigFile cf;
    cf.load(resources_file);

    Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();

    Ogre::String secName, typeName, archName;
    while (seci.hasMoreElements()) {
      secName = seci.peekNextKey();
      Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
      Ogre::ConfigFile::SettingsMultiMap::iterator i;
      for (i = settings->begin(); i != settings->end(); ++i) {
        typeName = i->first;
        archName = i->second;
        Ogre::ResourceGroupManager::getSingleton()
          .addResourceLocation(archName, typeName, secName);
      }
    }

    Ogre::ResourceGroupManager::getSingleton()
      .initialiseAllResourceGroups();
  }

  void register_keybindings(WindowEventListener& listener) {
    listener.add_hook({OIS::KC_ESCAPE}, EventType::menu_event,
                      bind(&WindowEventListener::shutdown, &listener));
    listener.add_hook({OIS::KC_C}, EventType::menu_event,
                      bind(&Game::create_ball, this));
  }

  void updateDeltaTime() {
    Ogre::Real now = timer->getMilliseconds();
    deltaT = now - last;
    last = now;
  }

  void game_loop(WindowEventListener& listener) {
    while (!listener.exit_) {
      updateDeltaTime();
      listener.capture();
      listener.check_events();
      scene->stepSimulation(deltaT, 1);
      render();
    }
  }
};

int main (void)
{
  Game game;
  WindowEventListener listener(game.window);

  game.start(listener);
}
