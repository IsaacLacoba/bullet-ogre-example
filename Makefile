CXX=g++ -std=c++11 -ggdb
CXXFLAGS = $(shell pkg-config --cflags OGRE OIS bullet)
LDLIBS = $(shell pkg-config --libs OGRE OIS bullet)

main: main.cpp

clean:
	find -name *[~#] -delete
	rm -f config/game.log config/ogre.log main

#
